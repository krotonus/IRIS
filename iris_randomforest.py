# -*- coding: utf-8 -*-
"""
Created on Wed Oct  4 04:33:45 2017

@author: HOME
"""

# -*- coding: utf-8 -*-
#Import statements to import various packages
import numpy as np #This package is used for the fast and efficient matrix operations
import pandas as pd #This pacakage provides the containers, i.e. "Dataframes" that make it easier to work with data
from sklearn.model_selection import train_test_split #This module is used for splitting dataset into testing and training
from sklearn import preprocessing #We use the Scaler function to scale our data
from sklearn.ensemble import RandomForestClassifier #This is the Random Forest algorithm for predicting quantitative values.
from sklearn.pipeline import make_pipeline #This is used to build a ML pipeline for testing different algorithms.
from sklearn.model_selection import GridSearchCV #This is used to perform K-fold Cross validation on our Dataset.
from sklearn.metrics import mean_squared_error, r2_score, roc_auc_score, confusion_matrix #This modules provide us with metrics that evalute our models and the training steps
from sklearn.externals import joblib # Similar to pickling. More efficient with large numpy arrays. Also it sticks with the whole sklearn theme.

dataset_url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
data = pd.read_csv(dataset_url, names = ["sepal-length","sepal-width","petal-length","petal-width","class"])
Y = data['class'] # Alternative to data['quality']
X = data.drop("class", axis = 1)

#Now splitting the data into training and testing
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.2, random_state = 123, stratify = Y)
#the parameters here say that we are splitting the data into 20% test size and we introduce a seed to reproduce
#results again. Also we "stratify" before splitting.

scaler = preprocessing.StandardScaler().fit(X_train) # Using the Transformer API from Sklearn to scale the data in both training and testing

#Scaling and transforming both Training and testing Data
X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)

pipeline = make_pipeline(preprocessing.StandardScaler(), RandomForestClassifier(n_estimators=100))
#making a pipeline to do it for us instead of hardcoding the steps each time

hyperparameters = { "randomforestclassifier__max_features" : ["auto","sqrt","log2"],
                    "randomforestclassifier__max_depth" : [None,5,3,1]}

clf = GridSearchCV(pipeline, hyperparameters, cv=10)

clf.fit(X_train,Y_train) #We use the GridSearch to tune the hyperparameters
#We find the default params were actually the best option

clf.refit

y_pred = clf.predict(X_test)

#Now we use the metrics to evaluate the performance of the model we built
print("Confusion Matrix = ")
print(confusion_matrix(Y_test, y_pred))
print(roc_auc_score(Y_test, y_pred))

#Saving the model for future use
joblib.dump(clf, "rf_classifier.pkl")
