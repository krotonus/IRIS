# -*- coding: utf-8 -*-
'''Machine Learning Mastery Code-Along'''
#Complete by 5:00 pm today, Please

'''Importing various python packages'''
import pandas as pd #For data manipulatin as storing as Dataframes
from pandas.tools.plotting import scatter_matrix #For plotting scatter matrices of the features
import seaborn as sns # primary data visulation
import matplotlib.pyplot as plt# for customizing the visuals
from sklearn import model_selection
'''Importing various metrics to analyse the differences between the various models. '''
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
'''Importing Various ML algorithms for working on the IRIS data set '''
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

'''Importing the data set from local csv file. The names are given to the names parameter '''
df = pd.read_csv('iris.data', names = ["sepal-length","sepal-width","petal-length","petal-width","class"])
'''Summarize the dataset '''
df.describe()

'''fig, axs = plt.subplots(ncols = 4)
sns.boxplot(y='sepal-length', data = df, ax = axs[0])
sns.boxplot(y='sepal-width', data = df, ax = axs[1])
sns.boxplot(y='petal-length', data = df, ax = axs[2])
sns.boxplot(y='petal-width', data = df, ax = axs[3])'''
#Tried to plot each feature separately in a subplot. Couldn't do it. Need to figure this out.

'''The following workaround works well to. But its too easy.'''
sns.pairplot(data =df, hue = 'class')
'''Creating training and testing dataframes'''
X = df.drop('class',axis=1)
Y = df['class']
X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X,Y, test_size = 0.20, random_state = 7)
'''Data is split. Now we dont't know which ML-algorithms work better on the dataset. Hence we are going to train the model using multiple algorithms and check the accuracy to select the best model.'''



models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))
'''Training over multiple ML-algorithms'''


results = []
names = []
for name, model in models:
    kfold = model_selection.KFold(n_splits = 10, random_state = 7)
    cv_results = model_selection.cross_val_score(model, X_train, Y_train, cv = kfold, scoring = 'accuracy')
    model.fit(X_train, Y_train)
    Y_pred = model.predict(X_test)
    print("------------"+name+"------------")
    print(classification_report(Y_test, Y_pred))
    print("--------------------------------")
    results.append(cv_results)
    names.append(name)
    msg = "Cross-validation Score of %s %f (%f)" % (name, cv_results.mean(), cv_results.std())
    print(msg)
'''Now we obtain the results which we can use to determine which algorithm performs the best on our dataset'''
'''I actually see that the SVM perform better than KNN. Surprising'''
'''We can plot the results to confirm.'''
'''We can validate and print the classification report(to-do)''' #Done
